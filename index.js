/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 */
exports.helloWorld = (req, res) => {
    let message = req.query.message || req.body.message || 'Hello World with automatic deploy 2';
    res.status(200).send(message);
};
